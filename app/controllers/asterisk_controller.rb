class AsteriskController < ApplicationController

	before_filter :require_user

	# AJAX
	def dial
		asterisk = Asterisk.new
		asterisk.user = current_user
		@number = params[:phone]
		id = params[:id]
		actionParam = params[:dialTo]
		asterisk.phone_number = @number
		asterisk.callee_id = id
		@object

		
		@ami = RubyAsterisk::AMI.new("207.239.212.28",5038)
		@success = true

		if actionParam == "contact"
			@contact = Contact.find_by_id(id)
			@object = @contact
			asterisk.action = "contact"
			asterisk.name = @contact.full_name
		elsif actionParam == "lead"
			@lead = Lead.find_by_id(id)
			@object = @lead
			asterisk.action = "lead"
			asterisk.name = @lead.full_name
		else
			asterisk.action = "unset"
		end
			

		login = @ami.login("admin","fr33pBx")
		if login.success
			originate = @ami.originate(current_user.extension,"from-internal", @number, "1")
			if originate.success
				asterisk.success = @success = true
				asterisk.message = @message = originate.message
			else
				asterisk.success = @success = false
				asterisk.message = @message = originate.message
			end
		else
			asterisk.success = @success = login.success
			asterisk.message = @message = login.message
		end

		if asterisk.valid?
			asterisk.save
		end

		respond_to do |format|
			format.js { render :layout=>false }
		end
	end
end
