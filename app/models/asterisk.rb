class Asterisk < ActiveRecord::Base
	# attr_accessible :title, :body
	belongs_to  :user

	has_paper_trail :ignore => [ :subscribed_users ]
end
