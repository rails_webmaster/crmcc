class AddExtensionToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :extension, :string, :limit => 10
  end
end
