class AddExtraFieldsToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :id_client, :string, :limit => 16
    add_column :accounts, :account_type, :string, :limit => 64
    add_column :accounts, :county, :string, :limit => 32
  end
end
