class ChangeDobFieldTypeToLead < ActiveRecord::Migration
	def up
		change_table :leads do |t|
			t.change :date_of_birth, :date
		end
	end

	def down
		change_table :leads do |t|
			t.change :date_of_birth, :datetime
		end
	end
end
