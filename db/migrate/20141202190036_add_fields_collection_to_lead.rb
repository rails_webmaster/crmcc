class AddFieldsCollectionToLead < ActiveRecord::Migration
	def change
		add_column :leads, :work_phone, :string, :limit => 32
		add_column :leads, :other_phone, :string, :limit => 32
		add_column :leads, :date_of_birth, :datetime
	end
end
