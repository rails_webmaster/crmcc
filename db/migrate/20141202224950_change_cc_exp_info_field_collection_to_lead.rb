class ChangeCcExpInfoFieldCollectionToLead < ActiveRecord::Migration
	def up
		change_table :leads do |t|
			t.change :cc_exp_year, 		:string,	:limit => 4
			t.change :cc_exp_month, 	:string,	:limit => 2
		end
	end

	def down
		change_table :leads do |t|
			t.change :cc_exp_year, 		:integer, 	:defualt => Date.today.year
			t.change :cc_exp_month, 	:integer, 	:default => 0
		end
	end
end
