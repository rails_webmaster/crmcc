class ChangeFieldsCollectionToLead < ActiveRecord::Migration
  def up
	rename_column :leads, :phone, 	:home_phone
	rename_column :leads, :mobile, 	:mobile_phone
  end

  def down
  	rename_column :leads, :home_phone, 		:phone
	rename_column :leads, :mobile_phone, 	:mobile
  end
end
