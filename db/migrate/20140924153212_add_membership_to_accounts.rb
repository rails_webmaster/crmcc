class AddMembershipToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :subscription_type, :string, :limit => 16
    add_column :accounts, :date_membership, :date
    add_column :accounts, :status_membership, :string, :limit => 32
    add_column :accounts, :month_membership, :string, :limit => 16
  end
end
