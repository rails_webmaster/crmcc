class AddSubscriptionStatusToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :subscription_status, :string, :limit => 16
  end
end
