class AddSubscriptionDateToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :subscription_date, :date
  end
end
