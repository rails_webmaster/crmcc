class CreateAsterisks < ActiveRecord::Migration
	def change
		create_table :asterisks do |t|
			t.references	:user
			t.string		:action
			t.string		:callee_id
			t.string		:name
			t.string		:phone_number
			t.string		:success
			t.string		:message
			t.timestamps
		end
	end
end
