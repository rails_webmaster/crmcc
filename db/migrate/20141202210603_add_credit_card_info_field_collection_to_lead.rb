class AddCreditCardInfoFieldCollectionToLead < ActiveRecord::Migration
	def change
		add_column :leads, :cc_holder, 		:string, 	:limit => 50
		add_column :leads, :cc_number, 		:string, 	:limit => 16
		add_column :leads, :cc_exp_month, 	:integer, 	:default => 0
		add_column :leads, :cc_exp_year,	:integer, 	:defualt => Date.today.year
		add_column :leads, :cc_sec_code,	:string, 	:limit => 3
	end
end
