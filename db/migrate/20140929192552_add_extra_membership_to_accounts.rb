class AddExtraMembershipToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :subscription_price, :decimal, :precision => 12, :scale => 2
    add_column :accounts, :first_bill_date, :date
    add_column :accounts, :next_bill_date, :date
    add_column :accounts, :paid_through_date, :date
  end
end
